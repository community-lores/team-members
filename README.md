# Community Lores Team Members

This repository maintains the list of project team members for Community Lores, an independent project under Recap Time's project maintenance policies and community code of conduct. See the FAQs

Have more questions? [Submit a new issue](https://gitlab.com/community-lores/team-members/issues) in this repo's GitLab issue tracker or email us at <inquiries@community-lores.tk>.

## So what is Community Lores?

Community Lores is an project for making different wikis and documentations available not only in Markdown formats minus trackers and creepy ads, plus even in form of APIs and Node.js packages to programmatically access them on your JavaScript code (feel free to fork them and port into your favorite language).

Through we might be look like Internet Archive in some aspects, we'll never  wanted to replace it, the upstream sources, and even Wikipedia because we edit and improve content and even add more context.

## Current

### Project Coordinators

Project Coordinators are like community maintainers, plus they have the sysadmin powers (akin to GitLab group owners) to the `Community-Lores` GitLab SaaS namespace and other infrastructure related to Community Lores. The Recap Time squad members (slash The Pins Team members) has the full control to these (through not all members has owner permissions to limit attack surface), but usually have notice-first requirement and grace period before an destructive action is being acted out. Some of the project coordinators can be either an Community Hubs Network Council member or an Recap Time Squad member.

* Andrei Jiroh Halili (can be emailed personally at <ajhalili2006@gmail.com> and for work at <andreijiroh@madebythepins.tk>, soon to be <andreijiroh@thepinsteam.dev> in the future) - Recap Time squad BDFL / lead maintainer

### Community Maintainers

Community maintainers, also called as project maintainers, can push and merge to the default branch (and any additional protected branches and tags) of different repos, can do an force-push if they ever `git commit --amend` (or do other sorts of history editing) after pushing commits (through this is highly discouraged), and generally have voting rights in Community Hubs Network Council/Board elections (in an foreseekable future). Destructive actions require an authorization step from an project coordinator.

* Add your name here.

### Community Developers

Community developers, also known as project developers, can push to branches where people with push access (in GitLab, this is called Developer permission) to unprotected branches and tags and in protected branches an tags where both maintainers and developers can push. We usually give Developer permissions to new contributors on per-repo basis, and once enough activity has accumated in the past 3 months, they'll be promoted to have Developer-level prvilleges across the GitLab SaaS namespace.

* Add your name here.

## Alumi Members

TBD

## Applying for Community Maintainer/Developer or Project Coordinator

Each project in Recap Time has its own project coordinators and community maintainers/developers that communicate with the squad members regarding different manners, including:

* infrastracture-related stuff (DNS, Docker containers hosted on Railway, GitLab namespace adminstration)
* legalese and abuse (code of conduct violation reports, copyright stuff, etc.)
* status of the project (development activity, compliance with commit message style if any)
* project maintenance and other assets, among other things

You'll be also invited into our Internal Notifications and Downtime Notifications Telegram groups/Matrix chats, as where we usually announce important stuff for all project coordinators and even community devs and maintainers to see. It's optional through, but we recommend to join there.

### Requirements

To apply for being an community maintainer, you must be an active Community Developer within Community Lores GitLab group for atleast 6 months (including the months where you have `Developer` permission in individual repos, among other things, including:

* compliance with The Pins Team Community Code of Conduct in good standing
  * We may check your CoC compliance with other projects you're participating or affliated with, but we currently don't do legal checks (especially we don't have our own lawyers to check your police records) yet.
* has an GPG key to sign commits and tags, atleast one key currently active in the past 3 months in other open-source projects
* has enabled 2FA on your GitLab SaaS account, please use Authy, Microsoft Authenicator, 1Password, Bitwarden, or any TOTP app or password manager which offers TOTP secret backups (atleast in encrypted form) since we're in GitLab's Free plan and we (and GitLab Support) cannot help you if you lost your recovery codes and your SSH keys are borked/lost
* (optional, but recommended) has contributions in other open-source projects within the last 12 months and/or has years of 2+ experinence of maintaining open-source projects, even your own projects

If you want to be an project coordinator, you must be an active Community Maintainer within Community Lores GitLab group for atleast 9 months to an year, and you must comply with the same requirements as applying for an community maintainer, plus:

* has an Guilded.gg, Discord, Telegram or Matrix account, as project coordinators are being communicated with the Recap Time Squad members and other employees of The Pins Team (we're not yet an company yet, as of 2021)
  * We recommend having an Telegram account since Andrei Jiroh is mostly active there, although Discord is also an option for you.
  * We don't use Signal here, but if yur threat model is high, we suggest using Matrix, preferrly creating an EE2E Matrix room, since we cannot sign in on multiple devices if we ever use Signal without that cursed SIM card/number.

> Obviously, since Andrei Jiroh is an self-assigned BDFL at The Pins Team/Recap Time, he can promote and demote anybody at his discretion for any reason, but you'll be usually notified before the action could go in effect. Other team members and the Community Hubs Network Council can also promote/demote anybody as long atleast two-thirds are agreed with the action. Even through he has that power, he usually asks other team members for consensus.

### The Process

The easy way to join is through active participation and contribution in projects related to Commmunity Lores. Once your first MR is being merged, we give you the `Developer` role to work with other developers, through its your choice whetever you do work on respective repos' seperate branches on still use your own fork for submitting MRs.

If you're fit for being an community maintainer or even project coordinator, please say hi to [Andrei Jiroh](https://andreijiroh.rtapp.tk/contact), our lead project coordinator and Recap Time squad BDFL, over Telegram/Discord/Matrix/Twitter. Please make sure that:

* you're atleast 18+ to apply for being an project coordinator, because there are some instances that you may be promoted to being an squad member and may have more access to our infrastructure, especially those cursed user data
* your GitLab profile must be public, with required timezone settings and pronouns to use + optionally your private contributions counted

## Authorization Step for Maintainers

This documentation is currently an work in progress as stated. This will be in the Team Handbook once written.

## Consensus Seeking

This documentation is currently an work in progress as stated. This will be in the Team Handbook once written.
